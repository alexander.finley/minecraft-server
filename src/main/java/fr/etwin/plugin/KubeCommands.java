package fr.etwin.plugin;

import fr.etwin.utils.KubeConstants;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class KubeCommands {
    public static boolean dispatch(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) {
            sender.sendMessage("Are you a player ?");
            return false;
        }
        try {
            switch (command.getName()) {
                case "tpzone":
                    return teleportCommand(player, command, label, args, KubeConstants.ZONE_WIDTH);
                case "tpchunk":
                    return teleportCommand(player, command, label, args, KubeConstants.CHUNK_WIDTH);
                case "regenerate":
                    return regenerateCommand(player, command, label, args, KubeConstants.CHUNK_WIDTH);
            }
        } catch (Exception e) {
            sender.sendMessage("Command failed : " + e.getMessage());
        }
        return false;
    }

    public static boolean teleportCommand(Player sender, Command command, String label, String[] args, int multiplier) {
        if (args.length != 2) return false;

        int zX = Integer.parseInt(args[0]);
        int zY = Integer.parseInt(args[1]);
        sender.sendMessage(command.getName() + " : teleported to " + zX + ":" + zY);
        sender.teleport(new Location(sender.getWorld(),
                zX * multiplier + multiplier / 2,
                10,
                zY * multiplier + multiplier / 2
        ));

        return true;
    }

    public static boolean regenerateCommand(Player sender, Command command, String label, String[] args, int multiplier) {
        if (args.length != 0) return false;
        Chunk chunk = sender.getLocation().getChunk();

        // TODO: generate zone, or kube chunk instead of juste a Minecraft chunk
        sender.getWorld().regenerateChunk(chunk.getX(), chunk.getZ());
        sender.sendMessage(command.getName() + " : 16x16 minecraft chunk regenerated !");

        return true;
    }
}
