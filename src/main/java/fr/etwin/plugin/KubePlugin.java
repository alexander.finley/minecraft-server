package fr.etwin.plugin;

import fr.etwin.generator.providers.DatabaseChunkProvider;
import fr.etwin.generator.KubeBiomeProvider;
import fr.etwin.generator.KubeChunkGenerator;
import org.bukkit.Difficulty;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public class KubePlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        new KubeScoreboard(this);

        // Setup some game rules when world is ready
        getServer().getScheduler().scheduleSyncDelayedTask(this, () -> {
            for (World world : getServer().getWorlds()) {
                world.setDifficulty(Difficulty.PEACEFUL);
                world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
                world.setGameRule(GameRule.RANDOM_TICK_SPEED, 0);
                world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
                world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
                world.setGameRule(GameRule.FALL_DAMAGE, false);
                world.setGameRule(GameRule.FIRE_DAMAGE, false);
                world.setGameRule(GameRule.KEEP_INVENTORY, true);
            }
        }, 1);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return KubeCommands.dispatch(sender, command, label, args);
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        //return new KubeChunkGenerator(new KubeFile(Paths.get(getDataFolder().getAbsolutePath(), "chunk.bin").toAbsolutePath().toString()));
        return new KubeChunkGenerator(new DatabaseChunkProvider());
        //return new KubeChunkGenerator(new KubeSocket(0));
    }

    @Override
    public BiomeProvider getDefaultBiomeProvider(String worldName, String id) {
        return new KubeBiomeProvider();
    }
}