package fr.etwin.plugin;

import fr.mrmicky.fastboard.FastBoard;
import fr.etwin.utils.Coordinate;
import fr.etwin.utils.KubeConstants;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;

public class KubeScoreboard implements Listener {
    JavaPlugin plugin;
    private final Map<UUID, FastBoard> boards = new HashMap<>();

    // private DatabaseChunkProvider kdb;

    public KubeScoreboard(JavaPlugin plugin) {
        this.plugin = plugin;
        getServer().getPluginManager().registerEvents(this, this.plugin);

        // ScoreBoard init
        getServer().getScheduler().runTaskTimer(this.plugin, () -> {
            for (Map.Entry<UUID, FastBoard> entry : this.boards.entrySet()) {
                updateBoard(entry.getKey(), entry.getValue());
            }
        }, 0, 20);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        FastBoard board = new FastBoard(player);

        board.updateTitle(ChatColor.RED + "Kube");

        this.boards.put(player.getUniqueId(), board);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        FastBoard board = this.boards.remove(player.getUniqueId());

        if (board != null) {
            board.delete();
        }
    }

    private void updateBoard(UUID playerId, FastBoard board) {
        Player player = Bukkit.getPlayer(playerId);
        if (player == null) {
            board.updateLines("Unable to find player");
            return;
        }
        Location location = player.getLocation();

        Coordinate zone = KubeConstants.getZoneFromLocation(location);

        board.updateLines(
                "",
                "X: " + (int) Math.floor(location.getX()) + " ; Z: " + (int) Math.floor(location.getZ()),
                "Zone [" + zone.mX + "][" + zone.mY + "]",
                "",
                "Loué par : TODO",
                "TODO"
        );
    }
}
