package fr.etwin.generator;

import org.bukkit.block.Biome;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.WorldInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * We do not handle Biomes. Just set "Plains" everywhere.
 */
public class KubeBiomeProvider extends BiomeProvider {
    @Override
    public Biome getBiome(WorldInfo worldInfo, int i, int i1, int i2) {
        return Biome.PLAINS;
    }

    @Override
    public List<Biome> getBiomes(WorldInfo worldInfo) {
        List<Biome> biomeList = new ArrayList<>();
        biomeList.add(Biome.PLAINS);
        return biomeList;
    }
}
