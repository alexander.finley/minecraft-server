package fr.etwin.generator.providers;

import fr.etwin.generator.KubeChunkProvider;
import fr.etwin.utils.KubeChunk;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseChunkProvider implements KubeChunkProvider {
    // TODO: config file
    private static final String url = "jdbc:postgresql://localhost:5432/kube_db";
    private static final String user = "postgres";
    private static final String password = "postgres";

    private Connection connection = null;

    public DatabaseChunkProvider() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Failed to load PostgreSQL driver");
        }
    }

    public boolean connect() {
        try {
            if (connection != null && !connection.isClosed()) {
                return true;
            }
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            connection = null;
        }
        return connection != null;
    }

    public boolean close() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            connection = null;
            return false;
        }
        return true;
    }

    @Override
    public KubeChunk getChunk(int mX, int mY) {
        if (!this.connect()) {
            return null;
        }
        try {
            // TODO: Handle history
            String sqlQuery = """
                SELECT data, compressed, created_at FROM chunks
                WHERE mx = ? AND my = ?;
                """;
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, mX);
            statement.setInt(2, mY);
            ResultSet rs = statement.executeQuery();

            List<KubeChunk> chunks = new ArrayList<>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                // Ungenerated chunk
                if (data == null) return null;
                boolean compressed = rs.getBoolean("compressed");
                Date created_at = rs.getDate("created_at");
                chunks.add(new KubeChunk(mX, mY, data, compressed, created_at));
            }
            if (chunks.isEmpty()) return null;
            return chunks.get(0);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            connection = null;
        }
        return null;
    }
}
